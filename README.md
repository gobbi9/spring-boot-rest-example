# About

A small REST API Spring Boot project that could be used as a bootstrap project.

# References


## Gradle 4.0.2

- [Install SDK](http://sdkman.io/)  
`$ curl -s "https://get.sdkman.io" | bash`

- [Install Gradle](https://gradle.org/install/)  
`$ sdk install gradle 4.0.2`

- Check version  
	`$ gradle -v`

- First setup  
	`$ gradle wrapper`

- Building  
	`$ ./gradlew build`

- Generating .jar  
	`$ ./gradlew bootRepackage`   
	The generated .jar file can be found in `build/libs`.

## Spring
- [Spring Boot 1.5.6](http://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/htmlsingle/)
- [Spring Framework 4.3.10](http://docs.spring.io/spring/docs/4.3.10.RELEASE/spring-framework-reference/htmlsingle/)

## Used tools
1. IntelliJ IDEA
2. BitBucket
3. Java 8

# Running

## Linux
```bash
$ ./gradlew bootRun
```
## Windows
```dos
> gradlew bootRun
```

The application is supposed to respond at <http://localhost:8080>

# Running Tests
## Linux
```bash
$ ./gradlew test
```
## Windows
```dos
> gradlew test
```

# Examples
The `+` and `^` operators cannot be passed as URL parameters !  
- <http://localhost:8080/solve?expression=(5*10)/(2-9)>  
- <http://localhost:8080/save?expression=(5*10)/(2-9)>  
- <http://localhost:8080/save?expression=cos(pi)>  
- <http://localhost:8080/solve?expression=cos(pi)*cos(pi)-(-sin(pi)*sin(pi))>  
- <http://localhost:8080/show>  

The expressions are saved in the user session i.e. cookies and solved through the [exp4j](http://projects.congrace.de/exp4j) library.

# Importing to common IDEs

After cloning the repository, you can import it to an IDE:  
```
git clone git@bitbucket.org:gobbi9/spring-boot-rest-example.git
or
git clone https://gobbi9@bitbucket.org/gobbi9/spring-boot-rest-example.git
```  
All the Gradle's commands listed above work independently from the IDE used. Make sure the default java version of your system is the Java 8. I recommend using the latest version of the IDEs.  
## Eclipse

1. Install EGradle Plugin  
    1. Help > Eclipse Marketplace > Search "EGradle"  
2. (Optional) Install Spring Tools  
    1. Help > Eclipse Marketplace > Search "Spring Tools"  
2. File > Import > EGradle > Import gradle root project with all subprojects  
3. Choose project folder in "Gradle root project path".  
4. In "Call Type" choose "Gradle wrapper in root project" matching the operating system you are using.  
5. Start Validation to verify the gradle settings  
6. Finish  
7. In the Project Explorer, right click at project title, and then Configure > Convert to faceted form  
    1. Select only Java and make sure the version is the 1.8; then OK  
8. In the Project Explorer, right click at project title, and then "Refresh Eclipse Dependencies"  
9. (Optional) If you did the step 2, you can right click at project title, and then Spring Tools > Add Spring Project Nature  

## IntelliJ IDEA

1. At the Welcome Screen, click on "Import Project"  
    1. If you do not see the Welcome Screen, go to File > Close Project  
2. Choose the project's root folder  
3. Select "Import project from external model" and then choose "Gradle"; Next  
4. Choose "Use gradle wrapper task configuration"  
5. Make sure the "Gradle JVM" is set to Java 8 and then Finish  
6. Open any Java class inside the IDE to set the Project SDK to Java 8  

## NetBeans

1. Go to Tools > Plugins  
    1. Go to the "Installed" tab and see if the "Gradle Support" plugin is installed  
    2. If not, go to the "Available Plugins" tab and search for "Gradle Support" and install it. Be careful to *not* install the "Gradle JavaEE Support" plugin.  
2. File > Open Project and select the project's root folder  
   
# Interesting stuff

## Making aliases to command line arguments

Important links:  
- <https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html>  
- <http://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/htmlsingle/#howto-use-short-command-line-arguments>    


In the `application.properties` file one can set properties to the spring application through command line and access those inside the code through the `@Value` annotation. As an example, we could set the `server.port` property and several aliases in the following manner:

### Naive setup  
```properties
server.port=8080
```  
Running the jar: `java -jar *.jar --server.port=8091`

### Adding an alias  
```properties
server.port=${port:8080}
port=${p:8080}
```
Running the jar: `java -jar *.jar --server.port=8091` or `java -jar *.jar --port=8091`.  
Running the application without passing the parameter, makes the `server.port` assume its default value: `8080`.  

### Adding more aliases and a "local variable"

```properties
default.port = 8080
server.port=${port:${default.port}}
port=${p:${default.port}}
p=${s.port:${default.port}}
```
Running the jar: `java -jar *.jar --server.port=8091` or `java -jar *.jar --port=8091` or `java -jar *.jar --p=8091` or `java -jar *.jar --s.port=8091`.  
Running the application without passing the parameter, makes the `server.port` assume its default value: `8080`.
Defining the `default.port` property in the command line also works to set the `server.port` property. That means the commands `java -jar *.jar --server.port=8091` and `java -jar *.jar --default.port=8091` do the same in this case.   
The idea above can be applied to any property.  

> **ATTENTION**: the `default.port`, `port`, `p` and `s.port` properties are not spring properties, thus some IDEs might show a warning that must be just ignored.

### Equivalent YAML configuration
```yaml
default:
  port: 8080
server:
  port: ${port:${default.port}}
port: ${p:${default.port}}
p: ${s.port:${default.port}}
``` 

## More YAML configurations

See <http://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/htmlsingle/#boot-features-external-config-yaml> and <http://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/htmlsingle/#boot-features-profiles>.  

```yaml
# SERVER PORT
default:
  port: 8080
server:
  port: ${port:${default.port}}
port: ${p:${default.port}}
p: ${s.port:${default.port}}

# PROFILES
---
spring:
  profiles: development
default:
  port: 8000
---
spring:
  profiles: production
default:
  port: 8001
```

```
java -jar *.jar => binds to 8080
java -jar -Dspring.profiles.active=development *.jar => binds to 8000
java -jar -Dspring.profiles.active=production *.jar => binds to 8001
java -jar -Dspring.profiles.active=development *.jar --port=9000 => binds to 9000
```
or  
```
java -jar *.jar => binds to 8080
java -jar *.jar --spring.profiles.active=development => binds to 8000
java -jar *.jar --spring.profiles.active=production => binds to 8001
java -jar *.jar --spring.profiles.active=development --port=9000 => binds to 9000
```
It's also possible to:  
- Map `.yml` properties to POJOs  
- Have multiple profiles activated at the same time  
- [Add](http://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/htmlsingle/#boot-features-adding-active-profiles) profiles to another profile  

Be careful with the [diference](http://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/htmlsingle/#boot-features-external-config-vs-value) between `@ConfigurationProperties` and `@Value`. Beans without the `@Profile` annotation are always instantiated, that's why the `@Primary` annotation is necessary in the `DevelopmentConfiguration` and `ProductionConfiguration` classes.

## Logging

Main references:  
- [Default logging configuration from Spring Boot](https://github.com/spring-projects/spring-boot/blob/master/spring-boot/src/main/resources/org/springframework/boot/logging/logback/defaults.xml)  
- [Main spring boot logging documentation](http://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/htmlsingle/#boot-features-logging)     
- [Mapped Diagnostic Context(MDC) example](http://www.baeldung.com/mdc-in-log4j-2-logback)  
- [Difference between Markers and MDC applications](https://stackoverflow.com/questions/4165558/best-practices-for-using-markers-in-slf4j-logback)  
- [Different logs for different classes](https://stackoverflow.com/questions/21979699/log4j-2-adding-multiple-colors-to-console-appender)  
- [log4j2 Tutorial](https://springframework.guru/using-log4j-2-spring-boot/)  
- [log4j2 Manual](http://logging.apache.org/log4j/2.x/manual/)  
- [Enabling Asynchronous Logging](https://springframework.guru/asynchronous-logging-with-log4j-2/)  
- [Logger configuration for tests](https://stackoverflow.com/questions/23200891/how-to-use-multiple-configuration-files-for-log4j2) with [XInclude](https://issues.apache.org/jira/browse/LOG4J2-341) (XInclude does not work as expected the configuration file was copied)
Log lines that start with `*` belong to the project/development log, whereas the other ones contains logs from the others libraries. The development log has also a slightly different color scheme, so that in a console which supports ANSI colors, it's really easy to discern the development logs from the system ones. An example can be seen [here](http://i.imgur.com/Nq6rWNs.png). The lines with pink text starting with a `*` are logs written by me or being triggered by a class written by me.  
  
A custom rolling file logging was implemented, more information under the [documentation](https://logging.apache.org/log4j/2.x/manual/appenders.html#RollingFileAppender) and configuration file `log4j2-dev.xml`, the structure of the log folder is depicted below:  
```
logs
  |
  ... app.log    (holds the most recent development logs)
  ... system.log (holds the most recent system/framework logs)
  ... app
       |
       ... 2017-07 (these folders will contain up to ${amount-log-files} files with ${individual-log-size} each)
             |
             ...  2017-07.1.log.gz (files with a higher index are newer than files with a smaller index)
             ...  2017-07.2.log.gz
             ...  2017-07.3.log.gz
             (! CAREFUL: 2017-07.3.log.gz does NOT mean July 3rd, but the third log archive from that month,
             which can happen at any day of July, 2017, depending on many log events are generated)
       ... 2017-08
             .
             .
             .

  ... system
       |
       ... 2017-07
             |
             ...  2017-07.1.log.gz
             ...  2017-07.2.log.gz
             ...  2017-07.3.log.gz
       ... 2017-08
             .
             .
             .
   ... test
If more than ${amount-log-files} files with ${individual-log-size} size each need to be saved in a folder,
the older files from that folder will be deleted

```  
`${amount-log-files}` was defined to be 100 and `${individual-log-size}` 1 MB.  
It was also defined a logging configuration for testing, which at the moment is at `src/test/resources/log4j-dev.xml` and is a copy of `src/test/resources/log4j-dev.xml`. So changes made in the main file must be manually made in the second, since I could not find a way to extend the configurations using XInclude or by passing `-Dlog4j.configurationFile=src/main/resources/log4j2-dev.xml,src/test/resources/log4j2-dev.xml` as an argument and hoping for a sensible merge of the configuration files. The tests logs are saved in a folder called `logs/test` and have the same structure explained above. If the tests logs should NOT be separated, one must only delete `src/test/resources/log4j-dev.xml`. 

### Defining different logging configuration for different profiles

1. Add the new XML configuration named, for example, `log4j2-prod.xml` to `src/test/resources/` and `src/main/resources/`. Be careful to change the `log-path` property from the new file `src/test/resources/log4j2-prod.xml` to `log/test`.
2. Insert in `application.yml` the following code under the respective profile.  
```yaml
logging:
  config: classpath:log4j2-prod.xml
```


## BitBucket Pipelines and Deployment Configurations

1. [Enable BitBucket pipelines](https://bitbucket.org/gobbi9/spring-boot-rest-example/admin/addon/admin/pipelines/settings)  
2. Add the respective [SSH keys](https://bitbucket.org/gobbi9/spring-boot-rest-example/admin/addon/admin/pipelines/ssh-keys) from the deployment machine  
3. Add an [access key](https://confluence.atlassian.com/bitbucket/use-access-keys-294486051.html) to the deployment machine  
4. Add the [Gradle](https://confluence.atlassian.com/bitbucket/java-with-bitbucket-pipelines-872013773.html#JavawithBitbucketPipelines-BuildandtestGradleprojects) [bitbucket-pipelines.yml](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html) configuration file and [validate it](https://bitbucket-pipelines.atlassian.io/validator)  

> How to execute [multiple tasks](https://docs.gradle.org/current/userguide/userguide_single.html#sec:executing_multiple_tasks) in Gradle: Using the command `./gradlew test bootRepackage`, the .jar will be generated if the all the tests succeded. The `./gradlew build` command also does the trick. If you wish to know which tasks are executed you can issue the command as follows: `./gradlew build --info`  

By default the tests are executed without any profile, which can cause errors. To [prevent it](https://stackoverflow.com/a/39546166), build the project as follows:
```
./gradlew build -Pspring.profiles.active=production
```
To make sure everything is being executed as it should, run `./gradlew build --info -Pspring.profiles.active=production`.  
The steps 2 and 3 were not executed for this project, since there is no test environment available yet. Another interesting resource is being able to [debug the pipelines locally](https://confluence.atlassian.com/bitbucket/debug-your-pipelines-locally-with-docker-838273569.html).

## Enabling automatic reload with Gradle for development [\[1\]](https://dzone.com/articles/continuous-auto-restart-with-spring-boot-devtools)[\[2\]](https://blog.gradle.org/introducing-continuous-build)[\[3\]](https://docs.gradle.org/4.0.2/userguide/continuous_build.html)
  
**This configuration is IDE independent.**  
  
From [\[2\]](https://blog.gradle.org/introducing-continuous-build) we also have:  
> "Gradle doesn’t consider changes to your build logic when in continuous build mode. Build logic is created from `build.gradle`, `settings.gradle`, `gradle.properties` and other sources. If you make changes to your build scripts, you’ll have to exit continuous build and restart Gradle. Future versions of Gradle will make it easier to describe inputs to your build logic so that continuous build can work with this as well."  
  
In a terminal, run:  
```
./gradlew build --continuous -x jar -x test
```  
The `-x` excludes the tasks `jar` and `test` from the continuous build, because the `jar` task causes that the build to constantly detect changes in the project and thus refreshing way more than necessary. The `test` task should not be run every time there is a small change in the project.   
  
In another terminal, run:  
```
./gradlew bootRun --console plain
```
## Caching

The underlying chosen cache library was [Caffeine](https://github.com/ben-manes/caffeine). This choice was based on how easy it was to configure the cache environment. However, in a real application it's necessary to assess the pros and cons of each cache library and the needs of the application to better decide which cache framework to use. The cache examples written in this repository have the main purpose to illustrate all possible uses, since some examples are more complicated than they needed to be.  
  
Main references:

1. [Spring MVC Cache documentation](http://docs.spring.io/spring/docs/4.3.10.RELEASE/spring-framework-reference/htmlsingle/#cache)  
2. [Spring Boot Cache documentation](http://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/htmlsingle/#boot-features-caching)  
3. [Cache Tutorial](http://www.baeldung.com/spring-cache-tutorial)  
4. [Caffeine Configuration example](http://memorynotfound.com/spring-boot-caffeine-caching-example-configuration/)  
5. [Prepopulating the cache](https://stackoverflow.com/questions/27940704/how-to-load-cache-on-startup-in-spring)  
