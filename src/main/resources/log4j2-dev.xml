<?xml version="1.0" encoding="UTF-8"?>
<!-- every 60 seconds this will file will be reloaded if changed -->
<Configuration monitorInterval="60" name="MainLogConfiguration">
    <!-- USER DEFINED PROPERTIES -->
    <Properties>
        <!-- directory where the file logs are saved, it's relative to the project root -->
        <Property name="log-path">logs</Property>
        <!-- How big each log file can get -->
        <Property name="individual-log-size">1 MB</Property>
        <!-- How many of those files each folder will have -->
        <Property name="amount-log-files">100</Property>
        <!-- the log pattern for the files, which can be the same for development and system logs, since they are saved separatelly-->
        <Property name="file-log-pattern">%-5level %d{yyyy-MM-dd } %d{HH:mm:ss.SSS} [%-40.40c] - %msg%n</Property>
    </Properties>
    <Appenders>
        <!--
            I removed the thread entry, since it's not really being helpful
            %style{[%-20.20t]}{dim}
        -->

        <!-- system logs to be shown on a console -->
        <Console name="system-console-appender" target="SYSTEM_OUT">
            <PatternLayout pattern="  %highlight{%-5level} %style{%d{yyyy-MM-dd }}{dim} %style{%d{HH:mm:ss.SSS}}{yellow} %style{[%-40.40c{1.}]}{cyan} - %msg%n">
            </PatternLayout>
        </Console>
        <!-- application/development logs to be shown on a console -->
        <Console name="app-console-appender" target="SYSTEM_OUT">
            <PatternLayout pattern="%highlight{* %-5level} %style{%d{yyyy-MM-dd }}{dim} %style{%d{HH:mm:ss.SSS}}{yellow} %style{[%-40.40c]}{cyan} - %style{%msg%n}{bright,magenta}">
            </PatternLayout>
        </Console>
        <!--
        https://logging.apache.org/log4j/2.x/manual/appenders.html#RollingFileAppender
        Example of a Rolling file configuration:
            logs
              |
              ... app.log    (holds the most recent logs)
              ... system.log (holds the most recent logs)
              ... app
                   |
                   ... 2017-07 (these folders will contain up to ${amount-log-files} files with ${individual-log-size} each)
                         |
                         ...  2017-07.1.log.gz (files with a higher index are newer than files with a smaller index)
                         ...  2017-07.2.log.gz
                         ...  2017-07.3.log.gz
                         (! CAREFUL: 2017-07.3.log.gz does NOT mean July 3rd, but the third log archive from that month,
                         which can happen at any day of July, 2017, depending on many log events are generated)
                   ... 2017-08
                         .
                         .
                         .

              ... system
                   |
                   ... 2017-07
                         |
                         ...  2017-07.1.log.gz
                         ...  2017-07.2.log.gz
                         ...  2017-07.3.log.gz
                   ... 2017-08
                         .
                         .
                         .
         If more than ${amount-log-files} files with ${individual-log-size} size each need to be saved in a folder,
         the older files from that folder will be deleted
        -->
        <!-- application/development logs to be shown on a file -->
        <RollingFile name="app-file-appender" fileName="${log-path}/app.log"
                     filePattern="${log-path}/app/$${date:yyyy-MM}/%d{yyyy-MM}.%i.log.gz">
            <PatternLayout pattern="${file-log-pattern}">
            </PatternLayout>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1"/> <!-- every 1 unit of %d{yyyy-MM}, i.e. every month -->
                <SizeBasedTriggeringPolicy size="${individual-log-size}"/> <!-- or if a single file exceeds this size -->
            </Policies>
            <DefaultRolloverStrategy max="${amount-log-files}"/>
        </RollingFile>

        <!-- system logs to be shown on a rolling file -->
        <RollingFile name="system-file-appender" fileName="${log-path}/system.log"
                     filePattern="${log-path}/system/$${date:yyyy-MM}/%d{yyyy-MM}.%i.log.gz">
            <PatternLayout pattern="${file-log-pattern}">
            </PatternLayout>
            <Policies>
                <TimeBasedTriggeringPolicy />
                <SizeBasedTriggeringPolicy size="${individual-log-size}"/>
            </Policies>
            <DefaultRolloverStrategy max="${amount-log-files}"/>
        </RollingFile>

        <!-- system logs to be shown on a file -->
        <!--
        <File name="app-file-appender" fileName="${log-path}/app.log" >
            <PatternLayout pattern="%-5level %d{yyyy-MM-dd } %d{HH:mm:ss.SSS} [%-40.40c] - %msg%n">
            </PatternLayout>
        </File>
        -->
        <!-- application/development logs to be shown on a file -->
        <!--
        <File name="system-file-appender" fileName="${log-path}/system.log" >
            <PatternLayout pattern="%-5level %d{yyyy-MM-dd } %d{HH:mm:ss.SSS} [%-40.40c{1.}] - %msg%n">
            </PatternLayout>
        </File>
        -->
    </Appenders>
    <Loggers>
        <!-- it says that every class pertaining to the example package or a subpackage should use the appenders below -->
        <AsyncLogger name="example" level="debug" additivity="false">
            <AppenderRef ref="app-file-appender"/>
            <AppenderRef ref="app-console-appender"/>
        </AsyncLogger>

        <!-- a log example, that is not based on class names -->
        <!--
        <AsyncLogger name="Very Important Messages" level="debug" additivity="false">
            <AppenderRef ref="app-file-appender"/>
            <AppenderRef ref="app-console-appender"/>
        </AsyncLogger>
        -->

        <!-- it says that every class that is NOT in the example package or a subpackage should use the appenders below -->
        <AsyncRoot level="info">
            <AppenderRef ref="system-file-appender"/>
            <AppenderRef ref="system-console-appender"/>
        </AsyncRoot>
    </Loggers>
</Configuration>

<!--
 LEVELS (from highest to lowest):
 FATAL, ERROR, WARN, INFO, DEBUG, TRACE
 evel="info", for example, means the every level above will be logged, i.e., INFO, WARN, ERROR, FATAL
 -->