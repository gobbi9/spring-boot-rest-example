package example;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class ExpressionTester {
    public static void main(String[] args) {
        Expression e = new ExpressionBuilder("3 * sin(y) - 2 / (x - 2)")
                .variables("x", "y")
                .build()
                .setVariable("x", 2.3)
                .setVariable("y", 3.14);
        double result = e.evaluate();
        System.out.println("Result: " + result);

        Expression e2 = new ExpressionBuilder("4+9^2")
                .build()
                ;
        double result2 = e2.evaluate();
        System.out.println("Result: " + result2);

//        String[] els = "asdfjhasajsdghfasdfa".split(",");
//        for (String s : els)
//            System.out.println(s);
    }
}
