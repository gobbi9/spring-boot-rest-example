package example.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public interface Configurations {
    String sayType();
}
