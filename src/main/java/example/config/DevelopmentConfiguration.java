package example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;

@Configuration
@Profile("development")
@Primary //solve conflict with DefaultConfiguration
public class DevelopmentConfiguration implements Configurations{
    protected final Logger logger = LoggerFactory.getLogger(DevelopmentConfiguration.class);

    @PostConstruct
    public void init(){
        logger.warn("configuration instanceof {} ? {}", DevelopmentConfiguration.class.getSimpleName(), this instanceof DevelopmentConfiguration);
    }

    @Override
    public String sayType() {
        return "DEVELOPMENT";
    }
}
