package example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;

@Configuration
@Profile("production")
@Primary //solve conflict with DefaultConfiguration
public class ProductionConfiguration implements Configurations{
    protected final Logger logger = LoggerFactory.getLogger(ProductionConfiguration.class);

    @PostConstruct
    public void init(){
        logger.warn(String.format("configuration instanceof %s ? %b", ProductionConfiguration.class.getSimpleName(), this instanceof ProductionConfiguration));
    }

    @Override
    public String sayType() {
        return "PRODUCTION";
    }
}
