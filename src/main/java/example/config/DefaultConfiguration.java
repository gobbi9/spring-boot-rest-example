package example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class DefaultConfiguration implements Configurations{
    protected final Logger logger = LoggerFactory.getLogger(DevelopmentConfiguration.class);

    @PostConstruct
    public void init(){
        logger.warn(String.format("configuration instanceof %s ? %b", DefaultConfiguration.class.getSimpleName(), this instanceof DefaultConfiguration));
    }

    @Override
    public String sayType() {
        return "DEFAULT";
    }
}