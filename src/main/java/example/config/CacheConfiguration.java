package example.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.cache.support.CompositeCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.Arrays;

/**
 * Class that contains all cache related configuration.
 */
@Configuration
@EnableCaching
public class CacheConfiguration {

    /**
     * The main cache manager using <a href="https://github.com/ben-manes/caffeine">Caffeine</a>.
     */
    @Bean
    public CaffeineCacheManager caffeineCacheManager() {
        CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
        caffeineCacheManager.setCacheNames(
                Arrays.asList("expressionResult", "expressionStatus")
        );
        return caffeineCacheManager;
    }

    /**
     * Enables a no op cache to be used if switching environments or testing.
     * @return a object that encapsulates cacheManagers of possibly different types
     */
    @Bean
    @Primary
    public CompositeCacheManager cacheManager() {
        CompositeCacheManager compositeCacheManager = new CompositeCacheManager();
        compositeCacheManager.setCacheManagers(
                Arrays.asList(caffeineCacheManager())
        );
        compositeCacheManager.setFallbackToNoOpCache(true);
        return compositeCacheManager;
    }

}