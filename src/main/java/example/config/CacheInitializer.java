package example.config;

import example.MainController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class CacheInitializer {
    private final Logger logger = LoggerFactory.getLogger(CacheInitializer.class);

    @Value("${cache.expressions}")
    private String[] expressions;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private MainController mainController;

    @PostConstruct
    public void populateCache() {
        for (String expression : expressions) {
            logger.debug("'{}' cached", expression);
            // the condition "#expression.length() > 10" is also applied here, that means the expressions
            // with 10 characters or less are not cached
            cacheManager.getCache("expressions").put(expression, mainController.solve(expression));
            // calling the method like this: mainController.solve(expression), does not trigger the cache storage, explanation
            // at end of this section: http://docs.spring.io/spring/docs/4.3.10.RELEASE/spring-framework-reference/htmlsingle/#cache-annotation-enable
        }
    }

}
