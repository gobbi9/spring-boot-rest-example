package example.model;

public class Result {
    private String value;
    private boolean valid;

    public Result() {

    }

    public Result(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Result result = (Result) o;

        if (valid != result.valid) return false;
        return value.equals(result.value);
    }

    @Override
    public int hashCode() {
        int result = value.hashCode();
        result = 31 * result + (valid ? 1 : 0);
        return result;
    }
}
