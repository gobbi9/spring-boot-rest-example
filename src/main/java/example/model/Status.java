package example.model;

public class Status {
    private boolean status;

    public Status() {
    }

    public Status(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Status status1 = (Status) o;

        return status == status1.status;
    }

    @Override
    public int hashCode() {
        return (status ? 1 : 0);
    }
}
