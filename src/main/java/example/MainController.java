package example;

import example.config.Configurations;
import example.model.Result;
import example.model.Status;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.test.util.AopTestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

// TODO
//- Spring Boot
//        - Remove unnecessary JARs
//        - Make more tests

@RestController
public class MainController {
    private final Logger logger = LoggerFactory.getLogger(MainController.class);

    // uncomment the AsyncLogger with name="Very Important Messages" in log4f2-dev.xml to see it in action
    //private final Logger importantLogger = LoggerFactory.getLogger("Very Important Messages");

    @Autowired
    private Expressions expressions;

    @Autowired
    private Configurations configuration;

    @Value("${spring.profiles.active:#{'default profile'}}")
    private String activeProfile;

    @Value("${server.port}")
    private Integer port;

    @PostConstruct
    public void init(){
        logger.warn(String.format("Port: %d (%s) [%s]", port, activeProfile, configuration.sayType()));
        //importantLogger.warn(String.format("Port: %d (%s) [%s]", port, activeProfile, configuration.sayType()));
    }

    /**
     * @param expression A expression containing numbers and common operators like ()-*\/, for example (5-10)/2-9, + and ^ operators cannot be passed as URL parameters
     * @return the evaluated result of the expression
     */
    @RequestMapping(path = "/solve", method = RequestMethod.GET, produces = "application/json")
    @Cacheable(
            cacheNames = "expressionResult"
            ,key = "#expression"
            ,condition = "#expression.length() > 10"
            ,unless = "!#result.valid"
    )
    // equal to @Cacheable(cacheNames="expressions", condition=..., unless=...), since the method only has one argument
    // only expressions longer than 10 characters are cached
    // unless they are not valid. The return value of a function can only be accessed in the unless clause and it is always
    // called #result, it's a mere coincidence, that in this method both have the same name, however the "#expresion" variable
    // does in fact match with name of the parameter "String expression", Spring EL can know the name of the parameters
    // of a method through reflection, but not the name of local variables declared inside the method
    public Result solve(@RequestParam("expression") String expression) {
        Result result = new Result();
        Expression e;
        try {
            e = new ExpressionBuilder(expression).build();
            if (e.validate().isValid()) {
                result.setValid(true);
                result.setValue(e.evaluate()+"");
            }
            else
                result.setValue("Invalid expression");
        }
        catch (Exception exp){
            result.setValid(false);
            result.setValue("Error!");
        }

        logger.debug("/solve({}) = {}", expression, result.getValue());
        return result;
    }

    /**
     * @param expression A expression containing numbers and common operators like ()-*\/, for example (5-10)/2-9, + and ^ operators cannot be passed directly as URL parameters
     * @return if the expression is valid or not
     */
    @RequestMapping(path = "/validate", method = RequestMethod.GET, produces = "application/json")
    @Cacheable(cacheNames = "expressionStatus")
    public Status validate(@RequestParam("expression") String expression) {
        Expression e;
        Status status;
        try {
            e = new ExpressionBuilder(expression).build();
            status = new Status(e.validate().isValid());
        }
        catch (Exception exp){
            status = new Status(false);
        }
        logger.debug("/valid?({}) = {}", expression, status.isStatus());
        return status;
    }

    /**
     * @param expression A expression containing numbers and common operators like ()+-*\/^, for example (5-10)*2, + and ^ operators cannot be passed directly as URL parameters
     * @return true, if the expression was saved, false if otherwise
     */
    @RequestMapping(path = "/save", method = RequestMethod.GET, produces = "application/json")
    @CachePut(cacheNames = "expressionStatus")
    // unlike Cacheable, this method will be cached, but it will be always executed. Since the return value is the same
    // from the method 'validate' and they perform similar operations, the cache from this method can be used in the method above
    public Status saveExpression(@RequestParam("expression") String expression) {
        Expression e;
        Status status;
        try {
            e = new ExpressionBuilder(expression).build();
            status = new Status(e.validate().isValid());
            if (status.isStatus()) {
                expressions.getExpressions().add(expression);
                logger.debug("expression '{}' saved in the session", expression);
            }
            else
                logger.debug("expression '{}' could NOT be saved in the session, it's invalid", expression);
        }
        catch (Exception exp){
            status = new Status(false);
            logger.debug("expression '{}' could NOT be saved in the session, it caused an ERROR", expression);
        }

        return status;
    }

    /**
     * @return the list of expressions saved in the session
     */
    @RequestMapping(path = "/show", method = RequestMethod.GET, produces = "application/json")
    public Expressions showExpressions() {
        return (Expressions) AopTestUtils.getUltimateTargetObject(expressions); //workaround to prevent that spring serializes the proxy object
    }

    @RequestMapping(path = "/clear", method = RequestMethod.GET, produces = "application/json")
    @CacheEvict(cacheNames = {"expressionStatus", "expressionResult"}, allEntries = true)
    // calling this method will clear all entries of both caches, aside from clearing the session stored list of expressions
    public Status clearExpressions(){
        expressions.getExpressions().clear();
        logger.debug("List of expressions cleared.");
        return new Status(true);
    }


}
