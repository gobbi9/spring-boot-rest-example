package example;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(value="expressions")
@Scope(proxyMode= ScopedProxyMode.TARGET_CLASS, value="session")
/**
 * Class to store a list of expressions in strings.
 */
public class Expressions {

    private List<String> expressions;


    public Expressions() {
        this.expressions = new ArrayList<String>();
    }

    public List<String> getExpressions() {
        return expressions;
    }

    public void setExpressions(List<String> expressions) {
        this.expressions = expressions;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        Expressions expressions = (Expressions) obj;
        return this.expressions.equals(expressions);
    }
}
