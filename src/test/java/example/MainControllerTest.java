package example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import example.model.Result;
import example.model.Status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.PostConstruct;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// https://www.javacodegeeks.com/2013/06/testing-spring-session-scope.html

@RunWith(SpringRunner.class)
@WebMvcTest(MainController.class)
@ActiveProfiles(resolver = ProfileCustomResolver.class)
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static ObjectMapper mapper = new ObjectMapper();

    private static final Status trueStatus = new Status(true);
    private static final Result result = new Result("30.0");

    private static final String EXPRESSION = "(5+10)*2";

    private static String trueStatusJSON, resultJSON;

    @PostConstruct
    public void init(){
        try {
            result.setValid(true);
            trueStatusJSON = mapper.writeValueAsString(trueStatus);
            resultJSON = mapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            //e.printStackTrace();
        }
    }

    @Test
    public void solveTest() throws Exception {
        mockMvc
            .perform(get("/solve?expression=" + EXPRESSION).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().json(resultJSON));
    }

    @Test
    public void saveTest() throws Exception {
        MockHttpSession mocksession = new MockHttpSession();
        Expressions expressions = new Expressions();
        expressions.getExpressions().add(EXPRESSION);
        mockMvc
            .perform(get("/save?expression=" + EXPRESSION).session(mocksession))
            .andExpect(status().isOk())
            .andExpect(content().json(trueStatusJSON))
            //TODO verify the session bean, not working atm
            //.andExpect(model().attribute("expressions", expressions))
        ;
    }
}

