package example;

import org.springframework.test.context.ActiveProfilesResolver;

public class ProfileCustomResolver implements ActiveProfilesResolver {

    public String[] resolve(Class<?> testClass) {
        return System.getProperty("spring.profiles.active").split(",");
    }
}
